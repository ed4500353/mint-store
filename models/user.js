const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    phone: {
        type: String
    },
    birthday: {
        type: Date
    },
    password: {
        type: String,
        required: true,
    },
    cart: {
        items: [{
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product',
                required: true
            },
            quantity: {
                type: Number,
                required: true
            },
        }, ],
    },
    resetToken: {
        type: String,
    },
    resetTokenExpiration: {
        type: Date,
    },
}, {
    versionKey: false
})

userSchema.methods.addToCart = function (product) {

    const cartProductIndex = this.cart.items.findIndex((item) => {
        return item.productId.toString() === product._id.toString()
    })

    let newQuantity = 1;

    const updatedCartItem = [...this.cart.items];

    if (cartProductIndex === -1) {

        updatedCartItem.push({
            productId: product._id,
            quantity: newQuantity,
        })

    } else {
        newQuantity = this.cart.items[cartProductIndex].quantity + 1;
        updatedCartItem[cartProductIndex].quantity = newQuantity;
    }

    const updatedCart = {
        items: updatedCartItem,
    };

    this.cart = updatedCart;

    return this.save();

}

userSchema.methods.deleteProductFromCart = function (productId) {
    const updatedCartItem = this.cart.items.filter(item => {
        return item.productId.toString() !== productId.toString();
    })

    this.cart.items = updatedCartItem;

    return this.save();
}

userSchema.methods.clearCart = function () {
    this.cart = {
        item: []
    };
    return this.save();
}


module.exports = mongoose.model('User', userSchema);