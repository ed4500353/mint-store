const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    user: {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        email: {
            type: String,
            required: true
        }
    },
    products: [{
        product: {
            type: Object,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        }
    }],
    date: {
        type: Date,
        required: true
    },
    totalPrice: {
        type: String,
        required: true
    },
    receiver: {
        type: String,
        required: true
    },
    receiver_phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: '訂單已成立'
    }
})

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;