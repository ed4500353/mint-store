const Product = require('../models/product');
const nodemailer = require('nodemailer');
require('dotenv').config();
const {
    validationResult
} = require('express-validator');

const ITEMS_PER_PAGE = 6;
let totalItems = 0;
let totalPages = 0;

// 首頁 Home
exports.getIndex = (req, res, next) => {
    Product.find().limit(4).then((product) => {
        res.render('index', {
            title: '首頁',
            product: product,
            homeActive: true,
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
        })
    }).catch((e) => {
        const error = new Error(e);
        error.status = 500;
        error.message = '請稍後再試'
        return next(error);
    })
}

// 商品頁面 Shop
exports.getProduct = async (req, res, next) => {
    // 產品分類（預設為全部）
    const type = req.query.type || 'all';
    const minPrice = +req.query.minPrice || 0;
    const maxPrice = +req.query.maxPrice || Infinity;

    // 分頁數（預設為第一頁）
    const page = +req.query.page || 1;
    let data
    try {
        if (type == 'all') {
            data = await Product.find({
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }
            }).skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE);
            totalItems = await Product.find({
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }
            }).countDocuments();
            totalPages = Math.ceil(totalItems / ITEMS_PER_PAGE);
        } else {
            data = await Product.find({
                type,
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }
            }).skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE);;
            totalItems = await Product.find({
                type,
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }
            }).countDocuments();
            totalPages = Math.ceil(totalItems / ITEMS_PER_PAGE);
        }

        res.render('product', {
            title: '商品頁面',
            shopActive: true,
            product: data,
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
            totalItems,
            totalPages,
            hasNextPage: ITEMS_PER_PAGE * page < totalItems,
            hasPrevPage: page > 1,
            nextPage: page + 1,
            prevPage: page - 1,
            currentPage: page,
            type,
            minPrice,
            maxPrice,
        })

    } catch (e) {
        const error = new Error(e);
        error.status = 500;
        error.message = '查無商品或頁面已移除'
        return next(error);
    }
}

// 商品頁面 Detail
exports.getProductDetail = async (req, res, next) => {
    const {
        productID
    } = req.params

    try {
        const product = await Product.findById(productID);
        res.render('product-detail', {
            title: '商品頁面',
            product,
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
        })

    } catch (e) {
        const error = new Error(e);
        error.status = 500;
        error.message = '查無此商品或頁面已移除'
        return next(error);
    }
}

/**聯絡我們 */
exports.getContact = (req, res, next) => {
    res.render('contact', {
        title: '聯絡我們',
        contactActive: true,
        isLoggedIn: req.session.isLoggedIn,
        userInfo: req.user,
        errorMessages: req.flash('error')[0],
        successMessages: req.flash('success')[0],
    });
}

exports.postContact = (req, res, next) => {
    const {
        name,
        email,
        messages
    } = req.body;

    const data = {
        name,
        email,
        messages,
    };


    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('contact', {
            title: '聯絡我們',
            contactActive: true,
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
            errorMessages: errors.array()[0].msg,
            successMessages: req.flash('success')[0],
        })
    }



    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            type: "OAuth2",
            user: 'edwardsogood@gmail.com',
            clientId: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
            refreshToken: process.env.REFRESH_TOKEN,
            accessToken: process.env.ACCESS_TOKEN,
        }
    });

    transporter.sendMail({
        from: `"${data.name}<${data.email}>" <${data.email}>`, // sender address
        to: 'edwardsogood@gmail.com', // list of receivers
        subject: `Mint Studio 來自${data.name}留下的訊息`, // Subject line
        text: `<b>訊息：${data.messages}</b>`, // plain text body
        html: `<b>訊息：${data.messages}</b>`, // html body
    }).then(() => {
        req.flash('success', '訊息已送出');
        res.render('contact', {
            title: '聯絡我們',
            contactActive: true,
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
            errorMessages: req.flash('error')[0],
            successMessages: req.flash('success')[0],
        });
    }).catch((e) => {
        const error = new Error(e);
        error.status = 500;
        error.message = ''
        return next(error);
    });

}
/**聯絡我們-end */