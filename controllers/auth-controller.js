const bcrypt = require('bcryptjs');
const User = require('../models/user');
const {
    validationResult
} = require('express-validator');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
require('dotenv').config();


/**使用者登入 */
exports.getLogin = (req, res, next) => {
    if (!req.session.isLoggedIn) {
        res.render('auth/login', {
            title: '會員登入',
            errorMessages: req.flash('error')[0],
        });
    } else {
        res.redirect('member');
    }
}

exports.postLogin = async (req, res, next) => {

    const {
        email,
        password,
    } = req.body;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('auth/login', {
            title: '會員登入',
            errorMessages: errors.array()[0].msg,
        })
    }

    try {
        const user = await User.findOne({
            email
        });

        if (!user) {
            req.flash('error', '電子郵件或密碼錯誤');
            return res.status(422).render('auth/login', {
                title: '會員登入',
                errorMessages: req.flash('error')[0],
            })
        }
        // 確認密碼是否正確
        bcrypt.compare(password, user.password, (err, result) => {
            if (!result) {
                req.flash('error', '電子郵件或密碼錯誤');
                return res.status(422).render('auth/login', {
                    title: '會員登入',
                    errorMessages: req.flash('error')[0],
                })
            }
            // 登入狀態改為已登入
            req.session.isLoggedIn = true;
            // 將用戶資料儲存
            req.session.user = user;
            return req.session.save(err => {
                res.redirect('/member');
            });
        })

    } catch (e) {
        const error = new Error(e);
        return next(error);
    }
}
/**使用者登入-end */

/**忘記密碼 */
exports.getForgotPassword = (req, res, next) => {
    res.render('auth/forgotpassword', {
        title: '忘記密碼',
    });
}

exports.postForgotPassword = (req, res, next) => {
    const {
        email
    } = req.body;
    crypto.randomBytes(32, (err, buffer) => {
        if (err) {
            console.log(err);
            return res.redirect('/forgotpassword');
        }

        const token = buffer.toString('hex');

        User.findOne({
            email
        }).then(user => {
            if (!user) {
                return res.redirect('/forgotpassword');
            }
            user.resetToken = token
            user.resetTokenExpiration = Date.now() + 1000 * 60 * 60;

            return user.save().then(result => {
                res.redirect('/login');

                const transporter = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 465,
                    secure: true,
                    auth: {
                        type: "OAuth2",
                        user: 'edwardsogood@gmail.com',
                        clientId: process.env.CLIENT_ID,
                        clientSecret: process.env.CLIENT_SECRET,
                        refreshToken: process.env.REFRESH_TOKEN,
                        accessToken: process.env.ACCESS_TOKEN,
                    }
                });

                transporter.sendMail({
                    from: "Mint Studio<mint.studio@gmail.com>", // sender address
                    to: email, // list of receivers
                    subject: 'Mint Studio【密碼重設通知信】', // Subject line
                    text: `<b>點擊連結後重設密碼</b><br><a href="http://ec2-3-137-90-161.us-east-2.compute.amazonaws.com/resetpassword/${token}">重設密碼</a>`, // plain text body
                    html: `<b>點擊連結後重設密碼</b><br><a href="http://ec2-3-137-90-161.us-east-2.compute.amazonaws.com/resetpassword/${token}">重設密碼</a>`, // html body
                })


            });
        }).catch(err => {
            console.log(err);
        })
    })
}

exports.getResetPassword = (req, res, next) => {
    const {
        token
    } = req.params;

    User.findOne({
        resetToken: token,
        resetTokenExpiration: {
            $gt: Date.now()
        }
    }).then(user => {
        if (!user) {
            const error = new Error();
            error.httpStatusCode = 500;
            return next(error);
        }
        res.render('auth/resetpassword', {
            title: '重設密碼',
            userId: user._id.toString(),
            passwordToken: token,
        });
    }).catch(e => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    })

}

exports.postResetPassword = async (req, res, next) => {
    const {
        newPassword,
        userId,
        passwordToken
    } = req.body;

    const hashedPassword = await bcrypt.hash(newPassword, 12);

    console.log(hashedPassword);

    User.findOneAndUpdate({
        resetToken: passwordToken,
        resetTokenExpiration: {
            $gt: Date.now()
        },
        _id: userId
    }, {
        password: hashedPassword,
        resetToken: undefined,
        resetTokenExpiration: undefined
    }, {
        new: true
    }, ).then((user) => {
        console.log(user)
        return res.redirect('/login');
    }).catch(e => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    })

}

/**註冊 */
exports.getSignup = (req, res, next) => {

    console.log(req.flash.error);
    res.render('auth/signup', {
        title: '會員註冊',
        errorMessages: req.flash('error')[0],
    });
}

exports.postSignup = async (req, res, next) => {
    const {
        name,
        email,
        password,
        confirmPassword,
    } = req.body;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('auth/signup', {
            title: '會員註冊',
            errorMessages: errors.array()[0].msg,
        })
    }

    try {

        const user = await User.findOne({
            email
        })

        // 確認用戶是否已存在
        if (user) {
            console.log('用戶已存在');
            req.flash('error', '用戶已存在');
            return res.redirect('/signup');
        }

        // 將密碼加密後再新增到ＤＢ
        bcrypt.hash(password, 12).then(hashedPassword => {
            const user = new User({
                name,
                email,
                password: hashedPassword,
                cart: {
                    items: [],
                },
            });

            user.save().then((user) => {
                console.log(user)
                return res.redirect('/login');
            }).catch(e => {
                console.log('Error:', e);
            })
        })
    } catch (e) {
        const error = new Error(e);
        return next(error);
    }
}
/**註冊-end */

// 登出
exports.postLogout = (req, res, next) => {
    req.session.destroy((err) => {
        console.log(err);
        res.redirect('/');
    })
}

/**管理員登入 */
exports.getAdminLogin = (req, res, next) => {
    if (!req.session.isAdminLoggedIn) {
        return res.render('auth/admin-login', {
            title: '管理者登入',
            errorMessages: req.flash('error')[0],
        });
    }
    res.redirect('admin');
}

exports.postAdminLogin = (req, res, next) => {
    const {
        userID,
        password,
    } = req.body;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('auth/admin-login', {
            title: '管理者登入',
            errorMessages: errors.array()[0].msg,
        })
    }

    if (userID !== 'admin' || password !== 'admin') {
        req.flash('error', '帳號或密碼不正確')
        return res.status(422).render('auth/admin-login', {
            title: '管理者登入',
            errorMessages: req.flash('error')[0],
        })
    }
    req.session.isAdminLoggedIn = true;
    res.redirect('/admin');
}
/**管理員登入-end */