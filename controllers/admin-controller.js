const Product = require('../models/product');
const Order = require('../models/order');
const moment = require('moment');
const PDFDocument = require('pdfkit');
const fs = require('fs');
const path = require('path');
const fileHelper = require('../util/filehelper');
const {
    validationResult
} = require('express-validator');

exports.getIndex = async (req, res, next) => {
    const product = await Product.find();
    res.render('admin/index', {
        title: '後台管理',
        product: product,
        isAdminLoggedIn: req.session.isAdminLoggedIn,
    })
}

exports.getAddProduct = (req, res, next) => {

    res.render('admin/edit-product', {
        title: '新增商品',
        isAdminLoggedIn: req.session.isAdminLoggedIn,
        errorMessages: '',
    });
}

exports.postAddProduct = (req, res, next) => {
    const {
        title,
        price,
        description,
        type,
    } = req.body;

    const image = req.file;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('admin/edit-product', {
            title: '新增商品',
            isAdminLoggedIn: req.session.isAdminLoggedIn,
            errorMessages: errors.array()[0].msg,
        })
    }

    const product = new Product({
        title,
        price,
        description,
        type,
        image: image.filename,
    })

    product.save().then((result) => {
        res.redirect('/admin');
    }).catch((e) => {
        const error = new Error(e);
        return next(error);
    })

}

exports.getEditProduct = (req, res, next) => {
    const {
        productID
    } = req.params;

    Product.findById(productID).then((result) => {
        res.render('admin/edit-product', {
            title: '修改商品',
            product: result,
            editMode: true,
            isAdminLoggedIn: req.session.isAdminLoggedIn,
            errorMessages: '',
        });
    }).catch((e) => {
        const error = new Error(e);
        return next(error);
    })

}

/**修改商品 */
exports.putEditProduct = async (req, res, next) => {
    const {
        productID
    } = req.params;
    const {
        title,
        price,
        description,
        type,
    } = req.body;
    const image = req.file;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return Product.findById(productID).then((result) => {
            res.status(422).render('admin/edit-product', {
                title: '修改商品',
                product: result,
                editMode: true,
                isAdminLoggedIn: req.session.isAdminLoggedIn,
                errorMessages: errors.array()[0].msg,
            })
        })
    }

    // 判斷是否有上傳新圖片
    if (image) {
        // 讀取原本圖片路徑並刪除

        const product = await Product.findOne({
            _id: productID
        });
        fileHelper.deleteFile('public/images/' + product.image);

        // 更新商品資訊
        return Product.findOneAndUpdate({
            _id: productID
        }, {
            title,
            price,
            description,
            type,
            image: image.filename
        }, {
            new: true
        }, ).then(() => {
            res.redirect('/admin');
        }).catch((e) => {
            const error = new Error(e);
            return next(error);
        })
    }

    // 無上傳新圖片 直接更新商品資訊
    Product.findOneAndUpdate({
        _id: productID
    }, {
        title,
        price,
        description,
        type,
    }, {
        new: true
    }, ).then(() => {
        res.redirect('/admin');
    }).catch((e) => {
        const error = new Error(e);
        return next(error);
    })
}
/**修改商品-end */

exports.deleteProduct = async (req, res, next) => {
    const {
        productID
    } = req.params;

    try {
        const product = await Product.findOne({
            _id: productID
        })
        // 先刪除圖片檔
        fileHelper.deleteFile('public/images/' + product.image);
        // 再從資料庫刪除資料
        await Product.deleteOne({
            _id: productID
        })
        res.redirect('/admin');
    } catch (e) {
        const error = new Error(e);
        return next(error);
    }
}

/**訂單 */
exports.getOrder = async (req, res, next) => {
    const order = await Order.find().sort([
        ['date', -1]
    ]).populate('user');
    res.render('admin/order', {
        title: '訂單管理',
        isLoggedIn: req.session.isLoggedIn,
        userInfo: req.user,
        order,
        moment
    });
}

exports.getOrderDetail = async (req, res, next) => {
    const {
        orderId
    } = req.params;
    const invoiceName = 'Invoice' + '-' + orderId + '.pdf';
    const invoicePath = path.join('data', 'invoices', invoiceName);
    const fontsPath = path.join('fonts', 'msyh.ttf');

    const pdfDoc = new PDFDocument();
    const order = await Order.findOne({
        _id: orderId
    });

    pdfDoc.pipe(fs.createWriteStream(invoicePath));
    pdfDoc.pipe(res);
    pdfDoc.font(fontsPath).fontSize(25).text('Mint Studio 訂單明細');
    pdfDoc.text('---------------------------------------');
    pdfDoc.font(fontsPath).fontSize(16).text('購買人：' + order.user.email);
    order.products.forEach(prod => {
        pdfDoc.font(fontsPath).fontSize(14).text('品名：' + prod.product.title + '\n' + '數量：' + prod.quantity + '\n' + '價格：' + prod.product.price + '元');
        pdfDoc.text('-----------------------------');
    })
    pdfDoc.font(fontsPath).fontSize(18).text('總金額：' + order.totalPrice + '元');
    pdfDoc.text('---------------------------------------');
    pdfDoc.font(fontsPath).fontSize(16).text('收件人：' + order.receiver);
    pdfDoc.font(fontsPath).fontSize(16).text('聯絡電話：' + order.receiver_phone);
    pdfDoc.font(fontsPath).fontSize(16).text('地址：' + order.address);
    pdfDoc.end();
}
/**訂單-end */