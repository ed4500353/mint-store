const Order = require('../models/order');
const Product = require('../models/product');
const User = require('../models/user');
const moment = require('moment');
const {
    validationResult
} = require('express-validator');

exports.getIndex = (req, res, next) => {

    res.render('user/index', {
        title: '會員管理',
        isLoggedIn: req.session.isLoggedIn,
        userInfo: req.user,
    });
}

exports.getCart = async (req, res, next) => {

    req.user.populate('cart.items.productId')
        .execPopulate()
        .then((user) => {
            const products = user.cart.items;
            console.log(products);
            res.render('user/cart', {
                title: '購物車',
                isLoggedIn: req.session.isLoggedIn,
                userInfo: req.user,
                cartProducts: products,
            });
        }).catch((err) => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

exports.postCart = (req, res, next) => {
    const productId = req.body.productId;

    Product.findById(productId)
        .then((product) => {
            return req.user.addToCart(product);
        })
        .then((result) => {
            res.redirect('/cart');
        }).catch((err) => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });

}

exports.deleteCart = (req, res, next) => {
    const productId = req.body.productId;
    req.user.deleteProductFromCart(productId)
        .then((result) => {
            res.redirect('/cart');
        }).catch((err) => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });

}

exports.getOrder = async (req, res, next) => {
    const order = await Order.find({
        "user.userId": req.user._id
    });
    res.render('user/order', {
        title: '訂單管理',
        isLoggedIn: req.session.isLoggedIn,
        userInfo: req.user,
        order,
        moment
    });
}

exports.getUser = (req, res, next) => {
    res.render('user/user', {
        title: '個人資料修改',
        isLoggedIn: req.session.isLoggedIn,
        userInfo: req.user,
        errorMessages: req.flash('error')[0],
        successMessages: req.flash('success')[0],
        moment
    });
}

exports.putUser = (req, res, next) => {
    const {
        name,
        phone,
        birthday,
    } = req.body;

    // 驗證內容訊息
    const errors = validationResult(req);
    // 驗證後如有錯誤將會回傳錯誤訊息
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('user/user', {
            title: '個人資料修改',
            isLoggedIn: req.session.isLoggedIn,
            userInfo: req.user,
            errorMessages: errors.array()[0].msg,
            successMessages: req.flash('success')[0],
            moment
        })
    }

    User.findOneAndUpdate({
        email: req.user.email
    }, req.body, {
        new: true
    }, ).then((result) => {
        req.flash('success', '資料已更新');
        res.render('user/user', {
            title: '個人資料修改',
            isLoggedIn: req.session.isLoggedIn,
            userInfo: result,
            errorMessages: req.flash('error')[0],
            successMessages: req.flash('success')[0],
            moment
        })
    }).catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });

}

exports.postOrder = (req, res, next) => {

    const {
        receiver,
        receiver_phone,
        address
    } = req.body;

    req.user.populate('cart.items.productId')
        .execPopulate()
        .then((user) => {
            const products = user.cart.items.map(item => {
                return {
                    quantity: item.quantity,
                    product: {
                        ...item.productId._doc
                    }
                };
            });

            let totalPrice = 0;
            user.cart.items.forEach((item) => {
                totalPrice = totalPrice + item.productId.price * item.quantity;
            })

            const order = new Order({
                user: {
                    email: req.user.email,
                    userId: req.user._id
                },
                products,
                date: Date.now(),
                totalPrice,
                receiver,
                receiver_phone,
                address
            })
            return order.save();
        }).then(() => {
            return req.user.clearCart();
        })
        .then(() => {
            res.redirect('/order')
        }).catch((err) => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}