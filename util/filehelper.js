const fs = require('fs');

// 刪除檔案
const deleteFile = (filePath) => {
    fs.unlink(filePath, err => {
        if (err) {
            throw err;
        }
    });
};

module.exports = {
    deleteFile
};