const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin-controller');
const isAdminAuth = require('../middleware/isAdminAuth');
const {
    body
} = require('express-validator');

router.get('/', isAdminAuth, adminController.getIndex);

router.get('/product', isAdminAuth, adminController.getAddProduct);

router.post('/product', isAdminAuth, [body('type').isLength({
        min: 1
    }).withMessage('請選擇商品類別'), body('title').trim().isLength({
        min: 1
    }).withMessage('商品名稱不得為空白'),
    body('price').trim().isNumeric({
        max: 99999
    }).custom((value, {
        req
    }) => {
        if (value < 0) {
            throw new Error('價格不可小於0')
        }
        return true;
    }).withMessage('價格僅限輸入0-99999之間的數字'),
    body('description').trim().isLength({
        max: 200
    }).withMessage('商品描述最多300字')
], adminController.postAddProduct);

router.get('/product/:productID', isAdminAuth, adminController.getEditProduct);

router.put('/product/:productID', [body('type').isLength({
        min: 1
    }).withMessage('請選擇商品類別'), body('title').trim().isLength({
        min: 1
    }).withMessage('商品名稱不得為空白'),
    body('price').trim().isNumeric({
        max: 99999
    }).custom((value, {
        req
    }) => {
        if (value < 0) {
            throw new Error('價格僅限輸入0-99999之間的數字')
        }
        return true;
    }).withMessage('價格僅限輸入0-99999之間的數字'),
    body('description').trim().isLength({
        max: 200
    }).withMessage('商品描述最多300字')
], isAdminAuth, adminController.putEditProduct);

router.delete('/product/:productID', isAdminAuth, adminController.deleteProduct);

router.get('/order', isAdminAuth, adminController.getOrder);

router.get('/order/:orderId', isAdminAuth, adminController.getOrderDetail);

module.exports = router;