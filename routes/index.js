const express = require('express');
const router = express.Router();
const shopController = require('../controllers/shop-controller');
const {
    body
} = require('express-validator');

/* GET home page. */
router.get('/', shopController.getIndex);

router.get('/contact', shopController.getContact);

router.post('/contact', [body('name').trim().isLength({
        min: 1
    }).withMessage('姓名不得為空白'),
    body('email').isEmail().trim().withMessage('請輸入正確電子郵件'),
    body('messages').trim().isLength({
        min: 5,
        max: 300
    }).withMessage('訊息內容不得為空 限制5-300字'),
], shopController.postContact);

router.get('/product', shopController.getProduct);

router.get('/product/:productID', shopController.getProductDetail);

module.exports = router;