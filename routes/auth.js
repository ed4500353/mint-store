const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth-controller');
const {
    body
} = require('express-validator');

/**管理者登入 */
router.get('/admin-login', authController.getAdminLogin);

router.post('/admin-login', [body('userID').trim().isLength({
        min: 1
    }).withMessage('帳號不得為空'),
    body('password').trim().isLength({
        min: 1
    }).withMessage('密碼不得為空')
], authController.postAdminLogin);
/**管理者登入-end */

/**會員登入 */
router.get('/login', authController.getLogin);

router.post('/login', [body('email').isEmail().withMessage('帳號不得為空且需為電子郵件'),
    body('password').trim().isLength({
        min: 1
    }).withMessage('密碼不得為空')
], authController.postLogin);
/**會員登入-end */

/**會員註冊 */
router.get('/signup', authController.getSignup);

router.post('/signup', [body('name').trim().isLength({
        min: 1
    }).withMessage('姓名不得為空'),
    body('email').isEmail().withMessage('帳號不得為空且需為電子郵件'),
    body('password').isAlphanumeric().isLength({
        min: 6
    }).withMessage('密碼需為英文或數字且至少6字元'),
    body('confirmPassword')
    .custom((value, {
        req
    }) => {
        if (value !== req.body.password) {
            throw new Error('確認密碼與密碼不相符')
        }
        return true;
    })
], authController.postSignup);
/**會員註冊-end */

router.get('/forgotpassword', authController.getForgotPassword);

router.post('/forgotpassword', authController.postForgotPassword);

router.get('/resetpassword/:token', authController.getResetPassword);

router.post('/resetpassword', authController.postResetPassword);


// 登出
router.post('/logout', authController.postLogout);

module.exports = router;