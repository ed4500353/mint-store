const express = require('express');
const router = express.Router();
const userController = require('../controllers/user-controller');
const isAuth = require('../middleware/isAuth');
const {
    body
} = require('express-validator');

router.get('/member', isAuth, userController.getIndex);

router.get('/cart', isAuth, userController.getCart);

router.post('/cart', isAuth, userController.postCart);

router.delete('/cart', isAuth, userController.deleteCart);

router.get('/order', isAuth, userController.getOrder);

router.get('/user', isAuth, userController.getUser);

router.put('/user', isAuth, [body('name').isLength({
            min: 1
        }).trim().withMessage('姓名不得為空'),
        body('phone').trim().isLength({
            min: 0,
            max: 10
        }).withMessage('聯絡電話格式不正確')
    ],
    userController.putUser);

router.post('/order', isAuth, userController.postOrder);

module.exports = router;